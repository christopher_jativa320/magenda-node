const express   = require('express');
const app       = express();

const http      = require('http').Server(app);
const path      = require('path');

const io        = require('socket.io')(http);


app.use(express.static(path.join(__dirname, 'public')));

io.on('connection', function(socket) {


    console.log('A user connected');
    socket.on('disconnect', function(){
        console.log('user disconnected');
      });

      // When we receive a 'message' event from the client, print out the contents of the message
      // then echo the message back to the client using 'io.emit()'
      socket.on('message', (message) => {
        console.log('Message received:' + message);
        io.emit('message', { type: 'new-message', text: message});
      });
});

http.listen(8080, function(){
    console.log('listening on *:8080');
    
  });